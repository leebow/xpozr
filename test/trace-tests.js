const expect = require('expect.js');
const Core = require('../lib/core');

describe('set-results-builder-tests', async () => {

    before('setup', async () => {

        this.__testObj = {
            getWidget: (id) => {
                return {
                    "id": id,
                    "name": "My test widget"
                }
            },
            testSyncFunc: (testArg) => {
                return {
                    testField: 'testValue1'
                }
            },
            testSyncFuncWithError: () => {
                throw new Error('test sync func error');
            },
            testCallBackFunc: (testArg, testArg2, callback) => {
                return callback(null, {
                    testField: 'testCallbackValue'
                });
            },
            testCallBackFuncWithError: (testArg, testArg2, callback) => {
                return callback(new Error('test callback func error'));
            },
            testCallBackFuncWithMultipleCallbackArgs: (testArg, callback) => {
                return callback(null,
                    {
                        testObj1Field: 'testObj1CallbackValue'
                    },
                    {
                        testObj2Field: 'testObj2CallbackValue'
                    });
            },
            testAsyncFunc: async () => {
                return {
                    testField: 'testValue2'
                }
            },
            testAsyncFuncWithError: async () => {
                throw new Error('test async func error');
            },
            testPromiseFunc: () => {
                return new Promise((resolve, reject) => {
                    resolve({
                        testField: 'testValue3'
                    })
                })
            },
            testPromiseFuncWithError: () => {
                return new Promise((resolve, reject) => {
                    return reject(new Error('test promise func error'));
                })
            }
        }

        this.__core = Core.create();
    });

    after('stop', async () => {
    });

    it('successfully wraps synchronous function', async () => {

        let wrapped = this.__core.trace(this.__testObj);

        let result = wrapped.testSyncFunc('joe');

        expect(result).to.eql(this.__testObj.testSyncFunc());
    });
    
    it('successfully wraps synchronous function and returns expected error', async () => {

        let wrapped = this.__core.trace(this.__testObj);

        try {
            wrapped.testSyncFuncWithError();
        } catch (err) {
            expect(err.message).to.equal('test sync func error');
        }
    });

    it('successfully wraps asynchronous function', async () => {

        let wrapped = this.__core.trace(this.__testObj);

        let result = await wrapped.testAsyncFunc();

        expect(result).to.eql(await this.__testObj.testAsyncFunc());
    });

    it('successfully wraps asynchronous function and returns expected error', async () => {

        let wrapped = this.__core.trace(this.__testObj);

        try {
            await wrapped.testAsyncFuncWithError();
        } catch (err) {
            expect(err.message).to.equal('test async func error');
        }
    });

    it('successfully wraps promise function', async () => {

        let wrapped = this.__core.trace(this.__testObj);

        wrapped.testPromiseFunc()
            .then(wrappedResult => {

                this.__testObj.testPromiseFunc()
                    .then(originalResult => {
                        expect(wrappedResult).to.eql(originalResult);
                    })

            });
    });

    it('successfully wraps promise function and returns expected error', async () => {

        let wrapped = this.__core.trace(this.__testObj);

        wrapped.testPromiseFuncWithError()
            .catch(wrappedResult => {

                this.__testObj.testPromiseFuncWithError()
                    .catch(originalResult => {
                        expect(wrappedResult).to.eql(originalResult);
                    })

            });
    });

    it('successfully wraps callback function', async () => {

        let wrapped = this.__core.trace(this.__testObj);

        let result1 = null;
        let result2 = null;

        let callback1 = (error, result) => {
            result1 = result;
        };

        let callback2 = (error, result) => {
            result2 = result;
        };

        // wrap these in async/await for testing
        (await (async () => { wrapped.testCallBackFunc('bob', {}, callback2) }))();
        (await (async () => { this.__testObj.testCallBackFunc('bob', {}, callback1) }))();

        expect(result1).to.eql(result2);

    });

    it('successfully wraps callback function and returns expected error', async () => {

        let wrapped = this.__core.trace(this.__testObj);

        let result1 = null;
        let result2 = null;

        let callback1 = (error) => {
            result1 = error;
        };

        let callback2 = (error) => {
            result2 = error;
        };

        // wrap these in async/await for testing
        (await (async () => { wrapped.testCallBackFuncWithError('bob', {}, callback2) }))();
        (await (async () => { this.__testObj.testCallBackFuncWithError('bob', {}, callback1) }))();

        expect(result1).to.eql(result2);

    });

    it('successfully wraps callback function with multiple callback arguments', async () => {

        let wrapped = this.__core.trace(this.__testObj);

        let result1 = null;
        let result2 = null;
        let result3 = null;
        let result4 = null;

        let callback1 = (error, res1, res2) => {
            result1 = res1;
            result2 = res2;
        };

        let callback2 = (error, res3, res4) => {
            result3 = res3;
            result4 = res4;
        };

        // wrap these in async/await for testing
        (await (async () => { wrapped.testCallBackFuncWithMultipleCallbackArgs('bob', callback2) }))();
        (await (async () => { this.__testObj.testCallBackFuncWithMultipleCallbackArgs('bob', callback1) }))();

        expect(result1).to.eql(result3);
        expect(result2).to.eql(result4);

    });
})