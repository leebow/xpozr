# xpozr

Super simple AOP-like tracing module for Javascript. 

Supports:
- Async functions
- Functions returning Promises
- Synchronous functions
- Synchronous functions with callbacks
  - supports callbacks with multiple result arguments (eg: `cb(err, result)`, `cb(err, result1, result2)` etc.)

## Usage

- A test class:

```
module.exports = class TestWidget {

    constructor(){}

    getWidget(id) {
        return {
            "id": id,
            "name": "My test widget"
        }
    }
}
```

- Create an instance of the object you would like to wrap, eg:

```
let widget = new TestWidget();
```

- Wrap the object, which will add extensive argument and result logging for every function in the object

```
const Xpozr = require('xpozr');
let xpozr = new Xpozr();

let wrappedWidget = xpozr.trace(widget);
```

- You can use the wrapped object exactly as you normally would:

```
let result = wrappedWidget.getWidget(2);
```

- The console output will be:

```
~~~~~~~~~~~~~~~~~~ START trace 'Object.getWidget' ~~~~~~~~~~~~~~~~~~
- function type: synchronous
- function name: getWidget
- start: 1588751615686
- end: 1588751615686
- duration:  0ms
- args:  [ 2 ]
- result:  { id: 2, name: 'My test widget' }
~~~~~~~~~~~~~~~~~~ END trace 'Object.getWidget' ~~~~~~~~~~~~~~~~~~
```

- See the tests (in `/test`) for more examples